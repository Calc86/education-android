package ru.ipartner.verstka

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ru.ipartner.verstka.model.Theme
import ru.ipartner.verstka.view.ThemeView

class Adapter(
    var clickListener : ((Theme) -> Unit)? = null,
    var viewListener : ((ThemeView.Action, Theme) -> Unit)? = null
) : RecyclerView.Adapter<Adapter.Holder>() {
    private val data : MutableList<Theme> = mutableListOf()

    fun setData(data : List<Theme>) {
        this.data.clear()
        this.data.addAll(data)
        notifyDataSetChanged()
    }

    private fun getItem(position : Int) : Theme = data[position]

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(ThemeView(parent.context).apply {
            this.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        })
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        val t = getItem(position)
        holder.bind(t, clickListener)
    }

    override fun getItemCount(): Int = data.size

    inner class Holder(var view: ThemeView) : RecyclerView.ViewHolder(view) {
        fun bind(data : Theme, listener : ((t : Theme) -> Unit)? = null) {
            view.setData(data)
            //view.data.set(data)
            view.setOnClickListener { listener?.invoke(data) }
            view.listener = { viewListener?.invoke(it, data) }
        }
    }
}
