package ru.ipartner.verstka

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ru.ipartner.verstka.view.MySnackBar2
import ru.ipartner.verstka.view.MySnackBarView
import ru.ipartner.verstka.view.ThemeView
import ru.ipartner.verstka.view.tag
import ru.ipartner.verstka.vm.ThemesViewModel

class MainActivity : AppCompatActivity() {
    private lateinit var list: RecyclerView
    private lateinit var viewSnack : MySnackBarView

    private val adapter: Adapter = Adapter()
    private val vm: ThemesViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        list = findViewById(R.id.rv)
        viewSnack = findViewById(R.id.viewSnack)



        list.layoutManager = LinearLayoutManager(this)
        list.adapter = adapter

        vm.loadThemes()
        vm.themes.observe(this) { adapter.setData(it) }

        setListeners()
    }

    private fun setListeners() = with(vm.events) {
        adapter.clickListener = { themeClick.value = it }

        adapter.viewListener = { action, theme ->
            when (action) {
                ThemeView.Action.MENU -> themeMenu.value = theme
            }
        }

        themeMenu.observe(this@MainActivity) {
            Log.d(tag(), "themeMenu click ${it.title}")
            viewSnack.setMessage(it.title)
            viewSnack.show()
        }

        themeClick.observe(this@MainActivity) {
            Log.d(tag(), "themeItem click ${it.title}")
        }

        viewSnack.listener = {
            when(it) {
                MySnackBarView.Action.CLOSE -> viewSnack.hide()
                MySnackBarView.Action.DONE -> viewSnack.hide()
            }
        }
    }
}
