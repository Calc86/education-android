package ru.ipartner.verstka.model

class Theme (
    var title : String,
    var test : Int,
    var sub_themes : Int,
    var blocks : Int,
    var blocks_count : Int,
    var status : String
)