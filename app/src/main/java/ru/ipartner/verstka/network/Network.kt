package ru.ipartner.verstka.network

import com.google.gson.Gson

import com.google.gson.reflect.TypeToken
import ru.ipartner.verstka.model.Theme
import java.lang.reflect.Type


class Network {

    private val data = "[\n" +
            "  {\n" +
            "    \"title\": \"Материалы ТЕХНОНИКОЛЬ, применяемые для фасадных систем утепления\",\n" +
            "    \"test\": 1,\n" +
            "    \"sub_themes\": 10,\n" +
            "    \"blocks\": 96,\n" +
            "    \"blocks_count\": 96,\n" +
            "    \"status\": \"open\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"title\": \"Материалы ТЕХНОНИКОЛЬ, систем утепления\",\n" +
            "    \"test\": 0,\n" +
            "    \"sub_themes\": 2,\n" +
            "    \"blocks\": 50,\n" +
            "    \"blocks_count\": 91,\n" +
            "    \"status\": \"open\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"title\": \"Материалы ТЕХНОНИКОЛЬ, применяемые для фасадных систем утепления\",\n" +
            "    \"test\": 1,\n" +
            "    \"sub_themes\": 51,\n" +
            "    \"blocks\": 0,\n" +
            "    \"blocks_count\": 92,\n" +
            "    \"status\": \"closed\"\n" +
            "  }\n" +
            "]"

    fun loadCards() : List<Theme> {
        val listType: Type = object : TypeToken<ArrayList<Theme>>() {}.type
        return Gson().fromJson(data, listType)
    }
}
