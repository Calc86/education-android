package ru.ipartner.verstka.view

import android.os.Build
import android.view.View
import androidx.annotation.ColorRes
import androidx.databinding.Observable

fun View.visible() {
    visibility = View.VISIBLE
}

fun View.invisible() {
    visibility = View.INVISIBLE
}

fun View.gone() {
    visibility = View.GONE
}

fun View.visibleIf(condition: Boolean) {
    visibility = if (condition) View.VISIBLE else View.GONE
}

fun View.getDefaultColor(@ColorRes id: Int): Int {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        resources.getColor(id, null)
    } else {
        resources.getColor(id)
    }
}

fun View.oneOfColor(condition: Boolean, @ColorRes idTrue: Int, @ColorRes idFalse: Int): Int {
    return if (condition) getDefaultColor(idTrue) else getDefaultColor(idFalse)
}

fun Any.tag(): String {
    return this.javaClass.toString()
}

fun getTextEnding(count: Int, norm: String, not: String, many: String): String {
    var ost = count % 100
    return if (ost in 10..19) {
        many
    } else {
        ost %= 10
        when (ost) {
            1 -> norm
            in 2..4 -> not
            else -> many
        }
    }
}

fun getTextEndingWithPattern(
    count: Int,
    pattern: String,
    norm: String,
    not: String,
    many: String,
): String {
    val end = getTextEnding(count, norm, not, many)
    return pattern.replace("%num%", count.toString()).replace("%text%", end)
}

/**
 * https://proandroiddev.com/the-ugly-onpropertychangedcallback-63c78c762394
 */
fun <T : Observable> T.addOnPropertyChanged(callback: (T) -> Unit) =
    addOnPropertyChangedCallback(
        object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(
                observable: Observable?, i: Int,
            ) =
                callback(observable as T)
        })