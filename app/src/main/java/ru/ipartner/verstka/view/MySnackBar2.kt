package ru.ipartner.verstka.view

import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import ru.ipartner.verstka.R

class MySnackBar2 @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr : Int = 0
) : FrameLayout(context, attrs) {

    private lateinit var tvMessage : TextView
    private lateinit var ivClose : ImageView
    private lateinit var tvAction : TextView

    var listener : ((Action) -> Unit)? = null

    enum class Action {
        CLOSE, ACTION
    }

    init {
        inflate(context, R.layout.view_my_shackbar, this)
        if(!isInEditMode) {
            tvMessage = findViewById(R.id.tvMessage)
            ivClose = findViewById(R.id.ivClose)
            tvAction = findViewById(R.id.tvAction)

            ivClose.setOnClickListener { listener?.invoke(Action.CLOSE) }
            tvAction.setOnClickListener { listener?.invoke(Action.ACTION) }
        }
    }

    fun setMessage(message : String) {
        tvMessage.text = message
    }
}
