package ru.ipartner.verstka.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import ru.ipartner.verstka.R
import ru.ipartner.verstka.databinding.ViewMyShackbarBinding

class MySnackBarView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0,
) : FrameLayout(context, attrs, defStyleAttr) {

    private val binding: ViewMyShackbarBinding =
        ViewMyShackbarBinding.inflate(LayoutInflater.from(context), this, true)
    var listener: ((Action) -> Unit)? = null

    init {
        with(binding) {
            tvAction.setOnClickListener { listener?.invoke(Action.DONE) }
            ivClose.setOnClickListener { listener?.invoke(Action.CLOSE)?: kotlin.run { hide() } }
            attrs?.let {
                context.obtainStyledAttributes(
                    it,
                    R.styleable.MySnackBarView,
                    0, 0
                ).apply {
                    try {
                        tvMessage.text = getString(R.styleable.MySnackBarView_msbv_message)?:"Тут будет сообщение"
                        tvAction.text = getString(R.styleable.MySnackBarView_msbv_button)?:"Кнопка"
                        if (!isInEditMode)
                            visibleIf(getBoolean(R.styleable.MySnackBarView_msbv_visible, false))
                    } finally {
                        recycle()
                    }
                }
            }
        }
    }

    enum class Action {
        CLOSE, DONE
    }

    fun setMessage(message: String) = with(binding) {
        tvMessage.text = message
    }

    fun show(animate: Boolean = false) {
        // todo add animation
        // todo add hide timeout
        visible()
    }

    fun hide(animate: Boolean = false) {
        // todo add animation
        gone()
    }
}
