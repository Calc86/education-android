package ru.ipartner.verstka.view

import android.content.Context
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import androidx.annotation.ColorRes
import androidx.databinding.ObservableField
import ru.ipartner.verstka.R
import ru.ipartner.verstka.databinding.ViewThemeBinding
import ru.ipartner.verstka.model.Theme

class ThemeView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0,
) : FrameLayout(context, attrs, defStyleAttr) {

    private lateinit var binding: ViewThemeBinding  // название биндинга формируется из layout/view_theme.xml

    @ColorRes
    private val colorGreyLight: Int = R.color.grey_light

    @ColorRes
    private val colorGrey: Int = R.color.grey

    @ColorRes
    private val colorBlack: Int = R.color.black

    @ColorRes
    private val colorRed: Int = R.color.red

    @ColorRes
    private val colorGreen: Int = R.color.green

    // обычный каллбек
    var listener: ((Action) -> Unit)? = null

    // эта штука чисто для примера, смотреть вскользь
    val data = ObservableField<Theme>()

    /**
     * набор действий для этого композитного View, нужен, если лень делать много листенеров
     */
    enum class Action {
        MENU
    }

    init {
        if (isInEditMode) {
            // эта ветка нужна для правильного отображения лейаута в превью AndroidStudio
            inflate(context, R.layout.view_theme, this)
        } else {
            binding = ViewThemeBinding.inflate(LayoutInflater.from(context), this, true)
            data.addOnPropertyChanged { cb -> cb.get()?.let { setData(it) } }
        }
    }

    fun setData(data: Theme) {
        if (isInEditMode) return
        with(binding) {
            tvTitle.text = data.title

            if(data.blocks == data.blocks_count) {
                imDone.visibility = View.GONE
            } else {
                imDone.visibility = View.VISIBLE
            }

            imDone.visibility = if(data.blocks == data.blocks_count) View.GONE else View.VISIBLE

            val doneCondition = data.blocks == data.blocks_count;

            imDone.visibleIf(doneCondition)
            //tvSubthemes.text = "${data.sub_themes} подтем"
            tvSubthemes.text = getTextEndingWithPattern(
                data.sub_themes, "%num% %text%",
                "подтема", "подтемы", "подтем"
            )
            progress.max = data.blocks_count
            progress.progress = data.blocks
            progress.visibleIf(data.blocks != data.blocks_count)

            tvInfo.visibleIf(data.test == 0)

//            tvBlocks.text = "${data.blocks}/${data.blocks_count} блоков"
            if (data.blocks == 0) {
                //tvBlocks.text = "${data.blocks_count} блоков"
                tvBlocks.text = getTextEndingWithPattern(
                    data.blocks_count, "%num% %text%",
                    "блок", "блока", "блоков"
                )
            } else {
                val blocksColor = oneOfColor(doneCondition, colorGreen, colorRed)

                val ssb = SpannableStringBuilder()
                ssb.append("${data.blocks}")
                ssb.setSpan(
                    ForegroundColorSpan(blocksColor),
                    0,
                    ssb.length,
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                )
                //ssb.append("/${data.blocks_count} блоков")
                ssb.append(
                    getTextEndingWithPattern(
                        data.blocks_count, "/%num% %text%",
                        "блок", "блока", "блоков"
                    )
                )
                tvBlocks.text = ssb
            }

            tvTitle.setTextColor(oneOfColor(data.blocks == 0, colorGreyLight, colorBlack))
            tvBlocks.setTextColor(
                oneOfColor(
                    doneCondition,
                    colorGrey,
                    colorGreyLight
                )
            )

            imMenu.setOnClickListener { listener?.invoke(Action.MENU) }
        }
    }
}
