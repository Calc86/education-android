package ru.ipartner.verstka.vm

import androidx.lifecycle.ViewModel
import ru.ipartner.verstka.network.Network

open class BaseViewModel : ViewModel() {
    //protected val compositeDisposable = CompositeDisposable()
    protected val network : Network = Network()

    override fun onCleared() {
        //compositeDisposable.dispose()
    }
}
