package ru.ipartner.verstka.vm

import androidx.lifecycle.MutableLiveData
import ru.ipartner.verstka.model.Theme

class ThemesViewModel : BaseViewModel() {
    val themes = MutableLiveData<List<Theme>>()
    val events = Events()

    inner class Events {
        val themeMenu  = SingleLiveEvent<Theme>()
        val themeClick = SingleLiveEvent<Theme>()
    }

    fun loadThemes() {
        themes.postValue(network.loadCards())
    }
}
